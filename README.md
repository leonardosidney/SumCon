# SumCon
a tribute to the game that was once played =D

Dependencies:
(Allegro 5)
liballegro-image5.0
liballegro-dialog5.0
liballegro-audio5.0
liballegro-acodec5.0
liballegro-ttf5.0
liballegro5.0

Code by Leonardo
Musics created by Lucas Vinicius
License: Creative Commons (by-sa)
