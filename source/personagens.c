#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

enum ids {personagem, inimigo, comidaRuim, comidaBoa, lutadores};
enum mov{x,y};
enum push{xp,yp};

typedef struct _hero {
    int id;
    int pos_x;
    int pos_y;
    float movimento;
    int borda_x;
    int borda_y;
    int vidas;
    int pontos;
    int pontosVida;
    bool ativo;
    bool desenha;
    int nivel;
    bool atualiza_pos;
    int mov[2];
    float push[2];
    int mv;
}Hero;
typedef struct _food {
    int id;
    int pos_x;
    int pos_y;
    float movimento;
    int borda_x;
    int borda_y;
    bool ativo;
    int direcao;
    int mov[2];
}Food;
typedef struct _fighter {
    Food runner;
    int equalize;
}Fighter;
