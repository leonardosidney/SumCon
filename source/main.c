#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_native_dialog.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_image.h>
#include "personagens.c"
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>
//Variaveis globais-----------------------
const int DISPLAY_Y = 600;
const int DISPLAY_X = 800;
const int FPS = 60.0;
enum TECLAS {UP,DOWN,LEFT,RIGHT,ENTER};
int NUM_FISH_MAX = 5;
int NUM_RICE_MAX = 15;
int NUM_APPLE_MAX = 15;
int RUNNERS_MAX = 8;
int STATS_SIZE = 100;

//________________________________________

//Prototipos ------------------------------
void StartHero(Hero *dragon_knight);
void DrawHero(Hero *dragon_knight, ALLEGRO_BITMAP *gordinho);
void MoveHeroUp(Hero *dragon_knight);
void MoveHeroDown(Hero * dragon_knight);
void MoveHeroLeft(Hero *dragon_knight);
void MoveHeroRight(Hero * dragon_knight);
void HeroCollision(Hero *dragon_knight, Food fish[],int max_fish,Food rice[],int max_rice, Food apple[],int max_apple,Fighter runners[], int max_runners,ALLEGRO_SAMPLE *morreu,ALLEGRO_SAMPLE_INSTANCE *som_morreu,ALLEGRO_SAMPLE *hurt,ALLEGRO_SAMPLE_INSTANCE *som_hurt);
void StartEnemy(Food enemy[],int max);
void ShootEnemies(Food enemy[],int max);
void RefreshEnemies(Food enemy[],int max);
void DrawFish(Food fish[],int max_fish,ALLEGRO_BITMAP *peixinho);
void DrawApple(Food apple[], int max_apple,ALLEGRO_BITMAP *maca);
void DrawRice(Food rice[], int max_rice,ALLEGRO_BITMAP *arroz);
void StartEnemyRunner(Fighter runners[], int max_runners);
void ShootRunners(Fighter runners[], int max_runners);
void RefreshRunners(Fighter runners[], int max_runners);
void DrawRunners(Fighter runners[], int max_runners);
void DrawStats(Hero *dragon_knight);
void CheckLevel(Hero *dragon_knight, Food fish[],int max_fish,Food rice[], int max_rice,Food apple[], int max_apple ,Fighter runners[], int max_runners,bool *level_up);
void Respaw(Hero *dragon_knight, int *i);
void Death(Hero *dragon_knight, bool *game_over);
void Game(ALLEGRO_DISPLAY *display, ALLEGRO_EVENT_QUEUE *event_queue, ALLEGRO_EVENT *event);
void Instrucoes(ALLEGRO_DISPLAY *tela,int addon);
ALLEGRO_BITMAP *RedimencionaPersonagem(ALLEGRO_BITMAP *gordinho, Hero *lutador);
ALLEGRO_BITMAP *RedimencionaAlimento(ALLEGRO_BITMAP *vegetal, Food *alimento);
void PushHero(Hero *dragon_knight);
void HeroOnMap(Hero *dragon_knight);
void RunnerCollision(Hero *dragon_knight, Food runner);
void MoveHero(Hero *dragon_knight);
void MoveEnemy(Food enemy[], int max);
//_________________________________________


int main() {
    //variaveis do jogo e do menu-----------------------
    ALLEGRO_EVENT_QUEUE *event_queue = NULL;
    ALLEGRO_TIMER *timer = NULL;
    ALLEGRO_DISPLAY *display = NULL;
    ALLEGRO_FONT *font14 = NULL;
    ALLEGRO_BITMAP *back = NULL;
    ALLEGRO_SAMPLE *opening = NULL;
    ALLEGRO_SAMPLE_INSTANCE *sound_opening = NULL;
    //_________________________________________
    //Inicializacao do allegro e do display -----
    if (!al_init()) {
        al_show_native_message_box(NULL,"Error", "Erro!", "Error to inicialize allegro!", NULL, ALLEGRO_MESSAGEBOX_ERROR);
        return -1;
    }
    display = al_create_display(DISPLAY_X, DISPLAY_Y);
    al_set_window_title(display, "SumCon");
    if (!display) {
        al_show_native_message_box(NULL, "Eror", "Error!", "Erroe to inicialize display", NULL, ALLEGRO_MESSAGEBOX_ERROR);
        return -1;
    }
    if (!(al_init_primitives_addon())) {
        al_show_native_message_box(NULL, "Error", "Error!", "Error to inicialize allegro_addons", NULL, ALLEGRO_MESSAGEBOX_ERROR);
        return -1;
    }
    if (!al_init_image_addon()){
        al_show_native_message_box(NULL, "Error", "Error!", "Error to inicialize allegro_image_addons", NULL, ALLEGRO_MESSAGEBOX_ERROR);
        return -1;
    }
    //___________________________________________
    //Fila de eventos e dispositivos------------
    event_queue = al_create_event_queue();
    timer = al_create_timer(1.0 /FPS);
    al_install_keyboard();
    al_install_mouse();
    al_init_image_addon();
    al_init_font_addon();
    al_init_ttf_addon();
    al_install_audio();
    al_init_acodec_addon();
    al_reserve_samples(30);
    font14 = al_load_font("../fonts/LiberationSans-Regular.ttf",70,0);
    //fundo = al_load_bitmap("sprites/fundo.png");
    opening = al_load_sample("../sounds/NoQuestions.ogg");
    sound_opening = al_create_sample_instance(opening);
    al_attach_sample_instance_to_mixer(sound_opening, al_get_default_mixer());
    al_set_sample_instance_playmode(sound_opening, ALLEGRO_PLAYMODE_LOOP);
    al_set_sample_instance_gain(sound_opening, 1.0);
    //__________________________________________
    //registro da fila de eventos-----------------
    al_register_event_source(event_queue, al_get_display_event_source(display));
    al_register_event_source(event_queue, al_get_timer_event_source(timer));
    al_register_event_source(event_queue, al_get_keyboard_event_source());
    //___________________________________________
    //inicializacoes------------------------------
    al_start_timer(timer);
    ALLEGRO_EVENT event;
    int cond = 0;
    bool game_over = false;
    //____________________________________________
    //Menu----------------------------------------
    while(!game_over) {
        al_wait_for_event(event_queue, &event);
        al_clear_to_color(al_map_rgb(255,255,255));
        //al_draw_bitmap(fundo, 0, 0, 0);
        al_draw_textf(font14,al_map_rgb(0,0,0), 290, 185, 0, "1. Play");
        al_draw_textf(font14,al_map_rgb(0,0,0),290, 245, 0, "2. Instructions");
        al_draw_textf(font14,al_map_rgb(0,0,0),290, 305, 0, "3. Credits");
        al_draw_textf(font14,al_map_rgb(0,0,0), 290, 365,0,"4 . Exit");
        al_flip_display();
        if(event.type == ALLEGRO_EVENT_DISPLAY_CLOSE) {
            game_over = true;
        }
        if(event.type == ALLEGRO_EVENT_KEY_DOWN) {
            switch (event.keyboard.keycode) {
            case ALLEGRO_KEY_ESCAPE:
                game_over = true;
                break;
            case ALLEGRO_KEY_1:
                cond = 1;
                break;
            case ALLEGRO_KEY_2:
                cond = 2;
                break;
            case ALLEGRO_KEY_4:
                game_over = true;
                break;
            }
        }
        if(event.type == ALLEGRO_EVENT_KEY_UP) {
            switch (event.keyboard.keycode) {
            case ALLEGRO_KEY_1:
                cond = 0;
                break;
            case ALLEGRO_KEY_2:
                cond = 0;
                break;
            }
        }
        if(event.type == ALLEGRO_EVENT_TIMER) {
            if(cond == 1) {
                cond = 0;
                al_stop_sample_instance(sound_opening);
                Game(display,event_queue, &event);
                al_flush_event_queue(event_queue);
            } else if(cond == 2) {
//                Instrucoes(&tela, &addon);
                al_flush_event_queue(event_queue);
            }
            al_play_sample_instance(sound_opening);
        }
    }
    //__________________________________________
    //finalizando programas ---------------------
    al_destroy_display(display);
    al_destroy_timer(timer);
    al_destroy_event_queue(event_queue);
    al_destroy_sample(opening);
    al_destroy_sample_instance(sound_opening);
    //__________________________________________
    //___________________________________________
    return 0;
}
void Game(ALLEGRO_DISPLAY *display, ALLEGRO_EVENT_QUEUE *event_queue, ALLEGRO_EVENT *event) {
    //variaveis do jogo -----------------------
    ALLEGRO_FONT *droid14 = NULL;
    ALLEGRO_SAMPLE *morreu = NULL;
    ALLEGRO_SAMPLE_INSTANCE *som_morreu = NULL;
    ALLEGRO_SAMPLE *musica_fundo = NULL;
    ALLEGRO_SAMPLE_INSTANCE *som_musica_fundo = NULL;
    ALLEGRO_SAMPLE *hurt = NULL;
    ALLEGRO_SAMPLE_INSTANCE *som_hurt = NULL;
    ALLEGRO_SAMPLE *powerup = NULL;
    ALLEGRO_SAMPLE_INSTANCE *som_powerup = NULL;
    ALLEGRO_BITMAP *gordinho = NULL;
    ALLEGRO_BITMAP *peixinho =NULL;
    ALLEGRO_BITMAP *maca =NULL;
    ALLEGRO_BITMAP *arroz =NULL;
    ALLEGRO_BITMAP *fundoJogo =NULL;
    srand(time(NULL));
    bool end = false, draw = true, keys[]= {false,false,false,false,false},level_up=false;
    bool game_over = false;
    //_________________________________________
    //Fila de eventos e dispositivos------------
    //tempoJogo = al_create_timer(1.0 /FPS);
    //al_install_keyboard();
    //al_init_font_addon();
    //al_init_ttf_addon();
    //al_install_audio();
    //al_init_image_addon();
    //al_init_acodec_addon();
    //al_reserve_samples(30);
    //_______________________________________________
    //IMAGENS DO GAME-------------------------------------

    droid14 = al_load_font("../fonts/DroidSans.ttf",14,0);
    /*
    gordinho = al_load_bitmap("sprites/personagem/g34.png");
    peixinho = al_load_bitmap("sprites/comidas/ComidaRuim30.png");
    maca = al_load_bitmap("sprites/comidas/ComidaBoa30.png");
    arroz = al_load_bitmap("sprites/comidas/ComidaEstragada30.png");
    fundoJogo = al_load_bitmap("sprites/back.png");
    //______________________________________________________
    //SONS DO GAME-----------------------------------------------------
    morreu = al_load_sample("sounds/Die.wav");
    musica_fundo = al_load_sample("sounds/ConSumoTheme.flac");
    som_morreu = al_create_sample_instance(morreu);
    al_attach_sample_instance_to_mixer(som_morreu, al_get_default_mixer());
    al_set_sample_instance_playmode(som_morreu,ALLEGRO_PLAYMODE_ONCE);
    al_set_sample_instance_gain(som_morreu, 1.0);
    som_musica_fundo = al_create_sample_instance(musica_fundo);
    al_attach_sample_instance_to_mixer(som_musica_fundo, al_get_default_mixer());
    al_set_sample_instance_playmode(som_musica_fundo, ALLEGRO_PLAYMODE_LOOP);
    al_set_sample_instance_gain(som_musica_fundo, 0.5);
    hurt = al_load_sample("sounds/Hurt.wav");
    som_hurt = al_create_sample_instance(hurt);
    al_attach_sample_instance_to_mixer(som_hurt, al_get_default_mixer());
    al_set_sample_instance_playmode(som_hurt,ALLEGRO_PLAYMODE_ONCE);
    al_set_sample_instance_gain(som_hurt, 1.0);
    powerup = al_load_sample("sounds/powerup.wav");
    som_powerup = al_create_sample_instance(powerup);
    al_attach_sample_instance_to_mixer(som_powerup, al_get_default_mixer());
    al_set_sample_instance_playmode(som_powerup,ALLEGRO_PLAYMODE_ONCE);
    al_set_sample_instance_gain(som_powerup, 1.0);
    */
    //__________________________________________
    //registro da fila de eventos-----------------
    //al_register_event_source(eventQueue, al_get_timer_event_source(tempoJogo));
    //al_register_event_source(eventQueue,al_get_keyboard_event_source());
    //___________________________________________
    //inicializacoes------------------------------
    //al_start_timer(tempoJogo);
    int respaw_time = 1;
    //____________________________________________
    //Criando objetos --------------------------
    Hero dragon_knight;
    Food fish[NUM_FISH_MAX];
    Food apple[NUM_APPLE_MAX];
    Food rice[NUM_RICE_MAX];
    Fighter runners[RUNNERS_MAX];
    //__________________________________________
    //arranque de funcoes -------------------------
    StartHero(&dragon_knight);
    StartEnemy(fish,NUM_FISH_MAX);
    StartEnemy(apple, NUM_APPLE_MAX);
    StartEnemy(rice, NUM_RICE_MAX);
    StartEnemyRunner(runners, RUNNERS_MAX);
    //al_play_sample_instance(som_musica_fundo);
    //____________________________________________
    while(!end) {
        al_wait_for_event(event_queue, event);
        //Eventos e logica do jogo -----------------
        if(event->type == ALLEGRO_EVENT_DISPLAY_CLOSE) {
            end = true;
        } else if (event->type == ALLEGRO_EVENT_TIMER) {
            if (keys[UP])
                MoveHeroUp(&dragon_knight);
            if (keys[DOWN])
                MoveHeroDown(&dragon_knight);
            if (keys[LEFT])
                MoveHeroLeft(&dragon_knight);
            if (keys[RIGHT])
                MoveHeroRight(&dragon_knight);
            if(game_over)
                //al_stop_sample_instance(som_musica_fundo);
            if(game_over && keys[ENTER]) {
                end = true;
            }
            if(!game_over) {
                draw = true;
                ShootEnemies(fish, NUM_FISH_MAX);
                RefreshEnemies(fish, NUM_FISH_MAX);
                ShootRunners(runners, RUNNERS_MAX);
                RefreshRunners(runners, RUNNERS_MAX);
                ShootEnemies(apple, NUM_APPLE_MAX);
                RefreshEnemies(apple, NUM_APPLE_MAX);
                ShootEnemies(rice, NUM_RICE_MAX);
                RefreshEnemies(rice, NUM_RICE_MAX);
                HeroCollision(&dragon_knight, fish, NUM_FISH_MAX,rice,NUM_RICE_MAX,apple,NUM_APPLE_MAX,runners,RUNNERS_MAX,morreu,som_morreu,hurt,som_hurt);
                PushHero(&dragon_knight);
                HeroOnMap(&dragon_knight);
                if (level_up) {
                    level_up = false;
                //al_play_sample_instance(som_powerup);
                }
                MoveHero(&dragon_knight);
                MoveEnemy(fish,NUM_FISH_MAX);
                MoveEnemy(apple,NUM_APPLE_MAX);
                MoveEnemy(rice,NUM_RICE_MAX);
                }
            } else if (event->type == ALLEGRO_EVENT_KEY_DOWN) {
                switch(event->keyboard.keycode) {
                case ALLEGRO_KEY_ESCAPE:
                    //al_stop_sample_instance(som_musica_fundo);
                    end = true;
                    break;
                case ALLEGRO_KEY_UP:
                    keys[UP] = true;
                    break;
                case ALLEGRO_KEY_DOWN:
                    keys[DOWN] = true;
                    break;
                case ALLEGRO_KEY_LEFT:
                    keys[LEFT] = true;
                    break;
                case ALLEGRO_KEY_RIGHT:
                    keys[RIGHT] = true;
                    break;
                case ALLEGRO_KEY_W:
                    keys[UP] = true;
                    break;
                case ALLEGRO_KEY_S:
                    keys[DOWN] = true;
                    break;
                case ALLEGRO_KEY_A:
                    keys[LEFT] = true;
                    break;
                case ALLEGRO_KEY_D:
                    keys[RIGHT] = true;
                    break;
                case ALLEGRO_KEY_ENTER :
                    keys[ENTER] = true;
                    break;
            }
        } else if (event->type == ALLEGRO_EVENT_KEY_UP) {
            switch(event->keyboard.keycode) {
                case ALLEGRO_KEY_UP:
                    keys[UP] = false;
                    break;
                case ALLEGRO_KEY_DOWN:
                    keys[DOWN] = false;
                    break;
                case ALLEGRO_KEY_LEFT:
                    keys[LEFT] = false;
                    break;
                case ALLEGRO_KEY_RIGHT:
                    keys[RIGHT] = false;
                    break;
                case ALLEGRO_KEY_W:
                    keys[UP] = false;
                    break;
                case ALLEGRO_KEY_S:
                    keys[DOWN] = false;
                    break;
                case ALLEGRO_KEY_A:
                    keys[LEFT] = false;
                    break;
                case ALLEGRO_KEY_D:
                    keys[RIGHT] = false;
                    break;
                case ALLEGRO_KEY_ENTER :
                    keys[ENTER] = false;
                    break;
            }
        }
        if (al_is_event_queue_empty(event_queue)) {
            Death(&dragon_knight, &game_over);
            CheckLevel(&dragon_knight,fish,NUM_FISH_MAX,rice,NUM_RICE_MAX,apple,NUM_APPLE_MAX,runners,RUNNERS_MAX,&level_up);
        }
        //Desenho da tela ---------------------------
        if (draw && al_is_event_queue_empty(event_queue)) {
            draw = false;
            if(!game_over) {
                //al_draw_bitmap(fundoJogo,0,100,0);
                DrawHero(&dragon_knight, gordinho);
                DrawFish(fish, NUM_FISH_MAX,peixinho);
                DrawApple(apple, NUM_APPLE_MAX,maca);
                DrawRice(rice, NUM_RICE_MAX,arroz);
                DrawRunners(runners, RUNNERS_MAX);
                DrawStats(&dragon_knight);
                Respaw(&dragon_knight, &respaw_time);
                al_draw_textf(droid14, al_map_rgb(255,255,255), 0, 0, 0, "LIFES %d /  SCORE %d / LEVEL %d", dragon_knight.vidas, dragon_knight.pontos, dragon_knight.nivel);
            } else {
                al_draw_textf(droid14, al_map_rgb(0,0,0), 250, 185, 0, "GAME OVER! PRESS ENTER TO RETURN");
                al_draw_textf(droid14, al_map_rgb(0,0,0),250, 205, 0, "SCORE: %d", dragon_knight.pontos);
            }
            al_flip_display();
            al_clear_to_color(al_map_rgb(255,255,255));
            //___________________________________________
        }
        //al_flush_event_queue(event_queue);
    }
    //al_destroy_timer(tempoJogo);
    //al_destroy_sample(morreu);
    //al_destroy_sample_instance(som_morreu);
    //al_destroy_sample(musica_fundo);
    //al_destroy_sample_instance(som_musica_fundo);
    //al_destroy_sample(hurt);
    //al_destroy_sample_instance(som_hurt);
    //al_destroy_sample(powerup);
    //al_destroy_sample_instance(som_powerup);
    //al_flush_event_queue(eventQueue);
}
void Intrucoes(ALLEGRO_DISPLAY *tela) {
    return;
}
//personagem ---------------------------------------
void StartHero(Hero *dragon_knight) {
    dragon_knight->pos_x = DISPLAY_X / 2.0;
    dragon_knight->pos_y = DISPLAY_Y / 2.0;
    dragon_knight->id = personagem;
    dragon_knight->vidas = 3;
    dragon_knight->movimento = 5;
    dragon_knight->pontos = 0;
    dragon_knight->borda_x = 15;
    dragon_knight->borda_y = 15;
    dragon_knight->pontosVida = 50;
    dragon_knight->ativo = true;
    dragon_knight->desenha = true;
    dragon_knight->nivel = 1;
    dragon_knight->atualiza_pos = false;
    dragon_knight->mov[x] = 0;
    dragon_knight->mov[y] = 0;
    dragon_knight->push[xp] = 0;
    dragon_knight->push[yp] = 0;
    dragon_knight->mv = 0;
}
void DrawHero(Hero *dragon_knight,ALLEGRO_BITMAP *gordinho) {
    if(dragon_knight->desenha) {
        //gordinho = RedimencionaPersonagem(gordinho, lutador);
        //al_draw_bitmap(gordinho,lutador->pos_x - lutador->borda_x, lutador->pos_y - lutador->borda_y,0);
        al_draw_filled_rectangle(dragon_knight->pos_x - dragon_knight->borda_x, dragon_knight->pos_y - dragon_knight->borda_y, dragon_knight->pos_x+dragon_knight->borda_x, dragon_knight->pos_y+dragon_knight->borda_y,al_map_rgb(100,100,100));
    }
}
void MoveHeroUp(Hero *dragon_knight) {
    dragon_knight->mov[y] -= dragon_knight->movimento;
}
void MoveHeroDown(Hero *dragon_knight) {
    dragon_knight->mov[y] += dragon_knight->movimento;
}
void MoveHeroLeft(Hero *dragon_knight) {
    dragon_knight->mov[x] -= dragon_knight->movimento;
}
void MoveHeroRight(Hero *dragon_knight){
    dragon_knight->mov[x] += dragon_knight->movimento;
}
void MoveHero(Hero *dragon_knight) {
    dragon_knight->pos_x += dragon_knight->mov[x];
    dragon_knight->mov[x] = 0;
    dragon_knight->pos_y += dragon_knight->mov[y];
    dragon_knight->mov[y] = 0;
}
void HeroCollision(Hero *dragon_knight, Food fish[], int max_fish, Food rice[], int max_rice, Food apple[],int max_apple,Fighter runners[],int max_runners,ALLEGRO_SAMPLE *morreu, ALLEGRO_SAMPLE_INSTANCE *som_morreu, ALLEGRO_SAMPLE *hurt, ALLEGRO_SAMPLE_INSTANCE *som_hurt) {
    int i;
    if(dragon_knight->ativo) {
        for (i = 0; i < max_apple; i++) {
        if(apple[i].ativo) {
            if(apple[i].pos_x - apple[i].borda_x < dragon_knight->pos_x + dragon_knight->borda_x &&
               apple[i].pos_x + apple[i].borda_x > dragon_knight->pos_x - dragon_knight->borda_x &&
               apple[i].pos_y - apple[i].borda_y < dragon_knight->pos_y + dragon_knight->borda_y &&
               apple[i].pos_y + apple[i].borda_y > dragon_knight->pos_y - dragon_knight->borda_y) {
                    apple[i].ativo = false;
                    dragon_knight->movimento += .1;
                    dragon_knight->pontosVida += 4;
                    dragon_knight->pontos += 1;
               }
            }
        }
        for (i = 0; i < max_rice; i++) {
            if(rice[i].ativo) {
                if(rice[i].pos_x - rice[i].borda_x < dragon_knight->pos_x + dragon_knight->borda_x &&
                   rice[i].pos_x + rice[i].borda_x > dragon_knight->pos_x - dragon_knight->borda_x &&
                   rice[i].pos_y - rice[i].borda_y < dragon_knight->pos_y + dragon_knight->borda_y &&
                   rice[i].pos_y + rice[i].borda_y > dragon_knight->pos_y - dragon_knight->borda_y) {
                        rice[i].ativo = false;
                        dragon_knight->pontosVida -= 8;
                        dragon_knight->movimento = 5;
                        if(dragon_knight->pontosVida <= 0) {
                            dragon_knight->vidas--;
                            dragon_knight->pontosVida = 1;
                            dragon_knight->ativo = false;
                            dragon_knight->atualiza_pos = true;
                            //al_play_sample_instance(som_morreu);
                        } else if(dragon_knight->pontosVida > 1) {
                            //al_play_sample_instance(som_hurt);
                        }
                   }
            }
        }
        for (i = 0; i < max_fish; i++) {
            if(fish[i].ativo) {
                if(fish[i].pos_x - fish[i].borda_x < dragon_knight->pos_x + dragon_knight->borda_x &&
                   fish[i].pos_x + fish[i].borda_x > dragon_knight->pos_x - dragon_knight->borda_x &&
                   fish[i].pos_y - fish[i].borda_y < dragon_knight->pos_y + dragon_knight->borda_y &&
                   fish[i].pos_y + fish[i].borda_y > dragon_knight->pos_y - dragon_knight->borda_y) {
                       fish[i].ativo = false;
                       dragon_knight->ativo = false;
                       dragon_knight->vidas--;
                       dragon_knight->movimento = 4.5;
                       dragon_knight->atualiza_pos = true;
                       //al_play_sample_instance(som_morreu);
                }
            }
        }
        for (i = 0; i < max_runners; ++i) {
            if (runners[i].runner.pos_x - runners[i].runner.borda_x < dragon_knight->pos_x + dragon_knight->borda_x &&
                runners[i].runner.pos_x + runners[i].runner.borda_x > dragon_knight->pos_x - dragon_knight->borda_x &&
                runners[i].runner.pos_y - runners[i].runner.borda_y < dragon_knight->pos_y + dragon_knight->borda_y &&
                runners[i].runner.pos_y + runners[i].runner.borda_y > dragon_knight->pos_y - dragon_knight->borda_y) {
                RunnerCollision(dragon_knight,runners[i].runner);
            }
        }
    }
}
void DrawStats(Hero *dragon_knight) {
    al_draw_filled_rectangle(0, 0, DISPLAY_X, STATS_SIZE,al_map_rgb(0,0,0));
    int life = (2 * dragon_knight->pontosVida);
    al_draw_rectangle(40 , 40 , 240, 60, al_map_rgb(255,0,0),1);
    al_draw_filled_rectangle(40 , 40 , 40 + life, 60, al_map_rgb(255,0,0));
}
void CheckLevel(Hero * dragon_knight,Food fish[],int max_fish,Food rice[], int max_rice,Food apple[], int max_apple,Fighter runners[], int max_runners,bool *level_up) {
    int i;
    if(dragon_knight->pontosVida >= 100) {
        dragon_knight->pontosVida = 1;
        dragon_knight->borda_x++;
        dragon_knight->borda_y++;
        dragon_knight->nivel++;
        *level_up = true;
        for(i = 0; i < max_apple; i++) {
            apple[i].borda_x ++;
            apple[i].borda_y ++;
            apple[i].movimento +=.02;
        }
        for (i = 0; i < max_rice; i++) {
            rice[i].borda_x ++;
            rice[i].borda_y ++;
            rice[i].movimento +=.02;
        }
        for(i = 0; i < max_fish; i++) {
            fish[i].borda_x ++;
            fish[i].borda_y ++;
            fish[i].movimento +=.02;
        }
        for (i = 0; i < max_runners; ++i) {
            runners[i].runner.borda_x ++;
            runners[i].runner.borda_y ++;
            runners[i].runner.movimento +=.02;
        }
    }
}
void Respaw(Hero *dragon_knight, int *i) {
    if(!dragon_knight->ativo) {
        if (*i * 5 % 2 == 0) {
            dragon_knight->desenha = true;
        } else {
            dragon_knight->desenha = false;
        }
        if(*i > 1.5 * FPS) {
            dragon_knight->ativo = true;
            dragon_knight->desenha = true;
            *i = 1;
        }
        *i= *i + 1;
    }
}
void Death(Hero *dragon_knight, bool *game_over) {
    if(dragon_knight->atualiza_pos) {
        dragon_knight->pos_x = DISPLAY_X / 2.0;
        dragon_knight->pos_y = DISPLAY_Y / 2.0;
        dragon_knight->atualiza_pos =false;
    }
    if(dragon_knight->vidas < 1) {
        *game_over = true;
    }
}
//______________________________________________________

//lutadores --------------------------------------------------
void StartEnemyRunner(Fighter runners[], int max_runners) {
    int i;
    for(i =0; i < max_runners; i++) StartEnemy(&runners[i].runner,1);
    runners->equalize = 0;
}
void ShootRunners(Fighter runners[], int max_runners) {
    int i;
    for (i = 0; i < max_runners; ++i) ShootEnemies(&runners[i].runner,1);
}
void RefreshRunners(Fighter runners[], int max_runners) {
    int i;
    for (i = 0; i < max_runners; i++) {
        RefreshEnemies(&runners[i].runner,1);
        MoveEnemy(&runners[i].runner,1);
    }
}
void DrawRunners(Fighter runners[], int max_runners) {
    int i;
    for(i = 0; i < max_runners; i++) {
        if (runners[i].runner.ativo) {
            //peixinho = RedimencionaAlimento(peixinho,inimigos);
            //al_draw_bitmap(peixinho,inimigos[i].pos_x -inimigos[i].borda_x,inimigos[i].pos_y -inimigos[i].borda_y,0);
            al_draw_filled_rectangle(runners[i].runner.pos_x -runners[i].runner.borda_x,runners[i].runner.pos_y -runners[i].runner.borda_y,runners[i].runner.pos_x + runners[i].runner.borda_x,runners[i].runner.pos_y+ runners[i].runner.borda_y,al_map_rgb(255,0,200));
        }
    }
}
// __________________________________________________________

//inimigos ----------------------------------------
void StartEnemy(Food enemy[],int max){
    int i;
    for(i = 0; i < max; i++) {
        enemy[i].id = inimigo;
        enemy[i].movimento = 2;
        enemy[i].ativo = false;
        enemy[i].borda_x = 5;
        enemy[i].borda_y = 5;
        enemy[i].mov[x] = 0;
        enemy[i].mov[y] = 0;
    }
}
void ShootEnemies(Food enemy[], int max){
    int chance = 500,i;
    for(i =0; i < max; i++) {
        if (!enemy[i].ativo) {
            if(rand() % chance == 0) {
                enemy[i].pos_x = DISPLAY_X;
                enemy[i].pos_y = STATS_SIZE + rand() % (DISPLAY_Y - 60);
                enemy[i].direcao = 1;
                // Direcao -> Direita para esquerda
                enemy[i].ativo = true;
            }else if(rand() % chance == 0) {
                enemy[i].pos_x = 0;
                enemy[i].pos_y = STATS_SIZE + rand() % (DISPLAY_Y - 60);
                enemy[i].direcao = 2;
                // Direcao -> Esquerda para direita
                enemy[i].ativo = true;
            } else if(rand() % chance == 0) {
                enemy[i].pos_x = 30 + rand() % (DISPLAY_X - 60);
                enemy[i].pos_y = DISPLAY_Y;
                enemy[i].direcao = 3;
                //Direcao -> Baixo para cima
                enemy[i].ativo = true;
            } else if(rand() % chance == 0) {
                enemy[i].pos_x = 30 + rand() % (DISPLAY_X - 60);
                enemy[i].pos_y = STATS_SIZE;
                enemy[i].direcao = 4;
                // Direcao -> Cima para baixo
                enemy[i].ativo = true;
            }
        }
    }
}
void MoveEnemy(Food enemy[], int max) {
    int i;
    for (i = 0; i < max; i++) {
        if(enemy[i].ativo) {
            if (enemy[i].direcao == 1) {
                enemy[i].mov[x] -= enemy[i].movimento;
            }else if(enemy[i].direcao == 2) {
                enemy[i].mov[x] += enemy[i].movimento;
            }else if(enemy[i].direcao == 3) {
                enemy[i].mov[y] -= enemy[i].movimento;
            }else if(enemy[i].direcao == 4) {
                enemy[i].mov[y] += enemy[i].movimento;
            }
        }
    }
}
void RefreshEnemies(Food enemy[], int max) {
    int i;
    for(i = 0;i < max;++i){
        if(enemy[i].ativo) {
            enemy[i].pos_x += enemy[i].mov[x];
            enemy[i].pos_y += enemy[i].mov[y];
            enemy[i].mov[x] = 0;
            enemy[i].mov[y] = 0;
            if(enemy[i].pos_y > DISPLAY_Y||enemy[i].pos_y < STATS_SIZE||
            enemy[i].pos_x > DISPLAY_X||enemy[i].pos_x < 0) {
                enemy[i].ativo = false;
            }
        }
    }
}

void DrawFish(Food fish[], int max_fish,ALLEGRO_BITMAP *peixinho){
    int i;
    for(i = 0; i < max_fish; i++) {
        if (fish[i].ativo) {
            //peixinho = RedimencionaAlimento(peixinho,inimigos);
            //al_draw_bitmap(peixinho,inimigos[i].pos_x -inimigos[i].borda_x,inimigos[i].pos_y -inimigos[i].borda_y,0);
            al_draw_filled_rectangle(fish[i].pos_x -fish[i].borda_x,fish[i].pos_y -fish[i].borda_y,fish[i].pos_x + fish[i].borda_x,fish[i].pos_y+ fish[i].borda_y,al_map_rgb(255,0,0));
        }
    }
}
//_________________________________________________________

void DrawApple(Food apple[], int max,ALLEGRO_BITMAP *maca) {
    int i;
    for(i = 0; i < max; i++) {
        if(apple[i].ativo) {
            //maca = RedimencionaAlimento(maca,comida);
            //al_draw_bitmap(maca,comida[i].pos_x - comida[i].borda_x, comida[i].pos_y - comida[i].borda_y,0);
            al_draw_filled_rectangle(apple[i].pos_x - apple[i].borda_x, apple[i].pos_y - apple[i].borda_y, apple[i].pos_x + apple[i].borda_x,apple[i].pos_y + apple[i].borda_y, al_map_rgb(0,255,0));
        }
    }
}
//__________________________________________________________

void DrawRice(Food rice[], int max,ALLEGRO_BITMAP *arroz) {
    int i;
    for(i = 0; i < max; i++) {
        if(rice[i].ativo) {
            //arroz = RedimencionaAlimento(arroz,ComidaEstragada);
            //al_draw_bitmap(arroz,ComidaEstragada[i].pos_x - ComidaEstragada[i].borda_x , ComidaEstragada[i].pos_y - ComidaEstragada[i].borda_y,0);
            al_draw_filled_rectangle(rice[i].pos_x -rice[i].borda_x , rice[i].pos_y - rice[i].borda_y, rice[i].pos_x + rice[i].borda_x,rice[i].pos_y + rice[i].borda_y, al_map_rgb(0,0,255));
        }
    }
}
ALLEGRO_BITMAP *RedimencionaPersonagem(ALLEGRO_BITMAP *gordinho, Hero *lutador) {
    ALLEGRO_BITMAP *bmpResize = NULL, *prev=NULL;
    bmpResize = al_create_bitmap((lutador->pos_x-lutador->borda_x), (lutador->pos_y-lutador->borda_y));
    prev = al_get_target_bitmap();
    al_set_target_bitmap(bmpResize);
    al_draw_scaled_bitmap(gordinho,
        0,0,
        al_get_bitmap_width(gordinho),
        al_get_bitmap_height(gordinho),
        0,0,
        lutador->borda_x,lutador->borda_y,
        0
    );
    al_set_target_bitmap(prev);
    //al_destroy_bitmap(gordinho);
    return bmpResize;
}
ALLEGRO_BITMAP *RedimencionaAlimento(ALLEGRO_BITMAP *vegetal, Food *alimento) {
    ALLEGRO_BITMAP *bmpResiz = NULL, *prev=NULL;
    bmpResiz = al_create_bitmap((alimento->pos_x-alimento->borda_x), (alimento->pos_y-alimento->borda_y));
    prev = al_get_target_bitmap();
    al_set_target_bitmap(bmpResiz);
    al_draw_scaled_bitmap(vegetal,
        0,0,
        al_get_bitmap_width(vegetal),
        al_get_bitmap_height(vegetal),
        0,0,
        alimento->borda_x,alimento->borda_y,
        0
    );
    al_set_target_bitmap(prev);
    //al_destroy_bitmap(gordinho);
    return bmpResiz;
}
void PushHero(Hero *dragon_knight) {
    //sem necessidade, bem parece que encontrei uma
    if(dragon_knight->mv > 0) {
        dragon_knight->pos_x += dragon_knight->push[xp];
        dragon_knight->pos_y += dragon_knight->push[yp];
        dragon_knight->mv--;
    }
}
void HeroOnMap(Hero *dragon_knight) {
    if (dragon_knight->pos_y < dragon_knight->borda_y + STATS_SIZE) {
        dragon_knight->pos_y = dragon_knight->borda_y + STATS_SIZE;
    }
    if (dragon_knight->pos_y > DISPLAY_Y - dragon_knight->borda_y) {
        dragon_knight->pos_y = DISPLAY_Y - dragon_knight->borda_y;
    }
    if (dragon_knight->pos_x < 0 + dragon_knight->borda_x) {
        dragon_knight->pos_x = 0 + dragon_knight->borda_x;
    }
    if (dragon_knight->pos_x > DISPLAY_X - dragon_knight->borda_x) {
        dragon_knight->pos_x = DISPLAY_X - dragon_knight->borda_x;
    }
}
void RunnerCollision(Hero *dragon_knight, Food runner) {
    //Não mecher até falar com o marco, vê se pode...
    //funciona
    //dá até vergonha upar isso
    //meu deus...
    //serio, tenho que arrumar isso...
    float x1,x2,y1,y2,vx,vy,vu;
    x1 = dragon_knight->pos_x;
    y1 = dragon_knight->pos_y;
    x2 = (dragon_knight->mov[x]);
    y2 = (dragon_knight->mov[y]);
    x2 *= 3;
    y2 *= 3;
    vx = x2 - x1;
    vy = y2 - y1;
    if(x2 != 0|| y2 != 0)vu = sqrt(pow(vx,2)+pow(vy,2));
    if(x2 > 0) {
        vx = (vx / vu) * 20;
    } else if (x2 < 0) {
        vx = (vx / vu) * -20;
    } else {
        vx = runner.mov[x]*4;
    }
    if(y2 > 0){
        vy = (vy / vu) * 20;
    } else if(y2 < 0){
        vy = (vy / vu) * -20;
    } else {
        vy = runner.mov[y]*4;
    }
    dragon_knight->push[xp] = vx;
    dragon_knight->push[yp] = vy;
    dragon_knight->mv = 10;
}
//__________________________________________________________
